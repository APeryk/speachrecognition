import {Component, OnInit} from '@angular/core';
import {SpeechRecognitionService} from '../shared/speech-recognition.service';
import {FireBaseApiService} from '../shared/fireBaseApi.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public listItems;
  public error: string;
  public started: boolean;

  constructor(private speechRecognition: SpeechRecognitionService,
              private fireBaseApiService: FireBaseApiService) {
  }

  ngOnInit() {
    this.speechRecognition.setSpeechParseResultListener((output) => {
      const speech = this.parseSpeech(output);
      if (speech) {
        this.fireBaseApiService.add(speech);
      } else {
        this.error = 'Будь-ласка спробуй щераз, ти сказав(ла): ' + output;
      }
    });
    this.fireBaseApiService.getDBdata()
      .subscribe(data => {
        this.listItems = data;
        console.log(data, '-test');
      });
  }

  delete(item) {
    this.fireBaseApiService.remove(item.id);
  }

  startStop() {
    if (this.started) {
      this.started = !this.started;
      this.speechRecognition.stop();
    } else {
      this.started = !this.started;
      this.error = null;
      this.speechRecognition.start();
    }
  }

  parseSpeech(output) {
    const parsed = {
      amount: '',
      typeOfDeal: '',
      currency: ''
    };

    if (output.match('куплю')) {
      parsed.typeOfDeal = 'Buy';
    }
    if (output.match('продам')) {
      parsed.typeOfDeal = 'Sell';
    }
    if (output.match('долар')) {
      parsed.currency = '$';
    }
    if (output.match('євро')) {
      parsed.currency = '€';
    }
    if (output.match(/\d+/)) {
      parsed.amount = output.match(/\d+/)[0];
    }

    console.log(output);
    return parsed.typeOfDeal && parsed.currency && parsed.amount && parsed;
  }
}
