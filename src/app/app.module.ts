import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {SpeechRecognitionService} from './shared/speech-recognition.service';
import {HomeComponent} from './home/home.component';
import {FireBaseApiService} from './shared/fireBaseApi.service';
import {ResolveService} from './shared/ResolveService';
import {SharedModule} from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule
  ],
  providers: [
    SpeechRecognitionService,
    ResolveService,
    FireBaseApiService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
