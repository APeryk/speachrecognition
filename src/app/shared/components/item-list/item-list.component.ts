import { Component, OnInit, Input } from '@angular/core';
import {FireBaseApiService} from '../../fireBaseApi.service';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {

  public filteredItems;

  @Input() listItems;
  constructor(private fireBaseApiService: FireBaseApiService) { }

  ngOnInit() {
    this.filteredItems = this.listItems;
  }

  delete(item) {
    this.fireBaseApiService.remove(item.id);
    const index = this.listItems.findIndex((element) => {
      return element.id === item.id;
    });
    this.listItems.splice(index, 1);
  }

  filterItems(filter) {
    this.filteredItems = this.listItems.filter(item => item.typeOfDeal === filter);
  }

  showAll() {
    this.filteredItems = this.listItems;
  }
}
