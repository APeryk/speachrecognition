import {Injectable} from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot
} from '@angular/router';
import {Observable, pipe} from 'rxjs';
import {map} from 'rxjs/operators';
import {FireBaseApiService} from './fireBaseApi.service';

@Injectable()
export class ResolveService implements Resolve<any> {

  constructor(private fireBaseApiService: FireBaseApiService) {}

  resolve(route: ActivatedRouteSnapshot,
  ): Observable<FireBaseApiService> {
    return this.fireBaseApiService.getData();
  }
}
