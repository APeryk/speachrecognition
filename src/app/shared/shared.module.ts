import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemListComponent} from './components/item-list/item-list.component';
import { MatButtonModule, MatButtonToggleModule} from '@angular/material';


@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatButtonToggleModule
  ],
  exports: [
    ItemListComponent,
    MatButtonModule,
    MatButtonToggleModule
  ],
  declarations: [
    ItemListComponent
  ]
})
export class SharedModule { }
