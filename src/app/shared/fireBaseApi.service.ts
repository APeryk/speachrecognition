import {Injectable} from '@angular/core';
import {Observable, Observer, of} from 'rxjs';

declare var firebase: any;

@Injectable()
export class FireBaseApiService {
  config: any = {
    apiKey: 'AIzaSyAuNm5CIHCsUsV3ENTtMnxFNElIiRxbM8k',
    authDomain: 'angular-course-b363c.firebaseapp.com',
    databaseURL: 'https://angular-course-b363c.firebaseio.com',
    projectId: 'angular-course-b363c',
    storageBucket: 'angular-course-b363c.appspot.com',
    messagingSenderId: '362166160084'
  };
  database: any;
  DBdata: Array<any> = [];

  constructor() {
    this.initDB();
  }

  initDB() {
    // Initialize Firebase
    firebase.initializeApp(this.config);

    firebase.auth().signInAnonymously()
      .catch(function (error) {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;

        console.log(`Auth error. errorCode - ${errorCode}, errorMessage - ${errorMessage}`);
      });

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.database = firebase.database();
      }
    });
  }

  getDBdata() {
    return Observable.create((observer: Observer<any>) => {
      const tobuys = firebase.database().ref('aPeryk/');

      tobuys.on('value', (tobuyList) => {
        this.DBdata = [];

        tobuyList.forEach(tobuy => {
          this.DBdata.push(tobuy.val());
        });
        observer.next(this.DBdata);
      });
    });
  }

  getData(): Observable<any> {
    return of(this.DBdata);
  }

  add(item) {
    const newId = +(new Date());

    item.id = newId;
    firebase.database().ref('aPeryk/' + newId).set(item);
  }

  update(id, item) {
    firebase.database().ref('aPeryk/' + id).update(item);
  }

  remove(id) {
    firebase.database().ref('aPeryk/' + id).remove();
  }
}
