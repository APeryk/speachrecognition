import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ResolveService} from './shared/ResolveService';

const routes: Routes = [
  {
    path: 'dolars',
    loadChildren: './dolars/dolars.module#DolarsModule',
    resolve: {
      itemList: ResolveService
    }
  },
  {
    path: 'euros',
    loadChildren: './euros/euros.module#EurosModule',
    resolve: {
      itemList: ResolveService
    }
  },
  {
    path: '',
    component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
