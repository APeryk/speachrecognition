import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EurosComponent } from './euros.component';

describe('EurosComponent', () => {
  let component: EurosComponent;
  let fixture: ComponentFixture<EurosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EurosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EurosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
