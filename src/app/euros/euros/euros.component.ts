import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-euros',
  templateUrl: './euros.component.html',
  styleUrls: ['./euros.component.css']
})
export class EurosComponent implements OnInit {

  public listItems: any = [];

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe(itemList => {
      this.listItems = itemList.itemList.filter(item => {
        return item.currency === '€';
      });
    });
  }
}
