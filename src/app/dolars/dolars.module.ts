import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DolarsRoutingModule } from './dolars-routing.module';
import { DolarsComponent } from './dolars/dolars.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    DolarsRoutingModule,
    SharedModule
  ],
  declarations: [DolarsComponent]
})
export class DolarsModule { }
