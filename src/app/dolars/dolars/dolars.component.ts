import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FireBaseApiService} from '../../shared/fireBaseApi.service';

@Component({
  selector: 'app-dolars',
  templateUrl: './dolars.component.html',
  styleUrls: ['./dolars.component.css']
})
export class DolarsComponent implements OnInit {

  public listItems: any = [];

  constructor(private route: ActivatedRoute,
              private fireBaseApiService: FireBaseApiService) { }

  ngOnInit() {
    this.route.data.subscribe(itemList => {
      this.listItems = itemList.itemList.filter(item => {
        return item.currency === '$';
      });
    });
  }
}
