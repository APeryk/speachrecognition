import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DolarsComponent } from './dolars.component';

describe('DolarsComponent', () => {
  let component: DolarsComponent;
  let fixture: ComponentFixture<DolarsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DolarsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DolarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
