import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DolarsComponent} from "./dolars/dolars.component";

const routes: Routes = [
  {
    path: '',
    component: DolarsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DolarsRoutingModule { }
